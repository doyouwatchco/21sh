/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/17 12:06:23 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:24:38 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stdlib.h>
# include <stdint.h>
# include <stddef.h>
# include <time.h>

# define LEN_(len)(sizeof(len) - 1)//Макросы для упрощения

# define PRTF_RESET			"\x1b[0m"//ft_printf define;

# define PRTF_BOLT			"\x1b[1m"
# define PRTF_INVERT		"\x1b[7m"

# define PRTF_BLACK			"\x1b[30m"
# define PRTF_BLACK_FON		"\x1b[40m"
# define PRTF_RED			"\x1b[31m"
# define PRTF_GREEN			"\x1b[32m"
# define PRTF_YELLOW		"\x1b[33m"
# define PRTF_BLUE			"\x1b[34m"
# define PRTF_PURPLE		"\x1b[35m"
# define PRTF_LIGHT_BLUE	"\x1b[36m"
# define PRTF_WHITE			"\x1b[37m"
# define PRTF_WHITE_FON		"\x1b[47m"

# define ITOA_LOWER			0x0//строчные буквы//ft_itoa define;
# define ITOA_UPPER			0x1//прописные буквы
# define ITOA_SIGNED		0x2//считает что получает базу с  отрицательными числами, если отрицательно ставит перед -
# define ITOA_GROUPED		0x4//групирует по три символа и разделяет пробелом
# define ITOA_PLUS			0x8//если установлен ITOA_SIGNED но число положительное то перед ним ставит +
# define ITOA_NOT_ONE		0x10//если установлен то к одному числу добавляеться ноль: 01

# define STR_UTF8_RIGHT		0x0//вправо//ft_str_utf8 define;
# define STR_UTF8_LEFT		0x1//влево

typedef struct			s_string
{
	size_t				len;
	size_t				max_len;
	char				*buff;
}						t_string;

typedef struct			s_write_buff//ft_write_buffer
{
	char				*b;//Указатель куда будем буферизовать
	size_t				max;//Максимально что вазможно записать в буффер
	size_t				count;//Текущее количество записанных данных в буффер //Изначально должно быть зануленно
	int					fd;//FD куда будем записывать
}						t_write_buff;

size_t					ft_strlen_utf8(register const char *s);
size_t					ft_strlen_utf8_n(register const char *s,
register const char *e);

void					ft_write_buffer_char(register t_write_buff *array,
register const char litter, register size_t n);
void					ft_write_buffer_str(register t_write_buff *array,
register const char *str, register size_t n);
void					ft_write_buffer_str_zero(register t_write_buff *array,
register const char *str);
void					ft_write_buffer(register t_write_buff *array);

char					**ft_strsplit(register char const *s, register char c);
void					ft_strsplit_free(register char **array);

char					*ft_strjoin(char const *s1, char const *s2);
int						ft_strcmp(register char *s1, register char *s2);//Standart functions
int						ft_strncmp(register char *s1, register char *s2,
register size_t n);
size_t					ft_strlen(register const char *s);
size_t					ft_wcslen(const wchar_t *s);
char					*ft_strdup(const char *s1);
char					*ft_strndup(const char *s1, size_t n);
char					*ft_strchr(const char *s, int c);
void					*ft_memset(void *b, int c, size_t len);
void					*ft_memcpy(void *dst, const void *src, size_t n);
int						ft_atoi(const char *str);
int						ft_itoa(char *buffer, uintmax_t value,
uint_fast8_t base, uint_fast8_t b_up);
int						ft_wctomb(char *s, wchar_t wchar);
struct tm				*ft_gmtime_r(const time_t *clock, struct tm *result);

#endif
