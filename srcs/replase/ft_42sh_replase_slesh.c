/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_replase_slesh.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

size_t			ft_42sh_replase_slesh_count(unsigned char **out,
register unsigned char *e, register unsigned char dquote)
{
	register unsigned char			*b;
	register size_t					count;
	register char					litter;

	b = *out;
	if (++b < e && b[0] == '\n')
	{
		count = 0;
		b++;
	}
	else
	{
		litter = b[-1];
		count = (dquote != 0 && litter != '\\') ? 1 : 0;
		if ((litter & 0x80) != 0)//Учитываем utf-8 что бы знать на сколько байт занимает одна буква
			while (((litter = litter << 1) & 0x80) != 0)
				b++;
		count += b - *out;
	}
	*out = b;
	return (count);
}

void			*ft_42sh_replase_slesh(register unsigned char *dest,
unsigned char **out, register unsigned char *e, register unsigned char dquote)
{
	register unsigned char			*b;
	register char					litter;
	register size_t					tempos;

	b = *out;
	if (++b < e && b[0] == '\n')
		b++;
	else
	{
		litter = b[-1];
		if (dquote != 0 && litter != '\\')
			dest++[0] = '\\';
		dest++[0] = litter;
		if ((litter & 0x80) != 0)//Учитываем utf-8 что бы знать на сколько байт нужно скопировать
		{
			tempos = 0;
			while (((litter = litter << 1) & 0x80) != 0)
				tempos++;
			while (tempos-- > 0)
				dest++[0] = b++[0];
		}
	}
	*out = b;
	return (dest);
}
