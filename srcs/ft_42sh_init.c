/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_init.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static size_t	fn_pwd(register t_string *lp)
{
	register void		*tmp;
	register size_t		tempos;

	if ((tmp = getcwd(0, 0)) == 0)//Получаем текущий путь из какого стартуем свою работу
		return (0);
	lp->buff = tmp;
	tempos = ft_strlen(tmp);
	lp->len = tempos;
	lp->max_len = tempos + 1;
	return (ft_strlen_utf8(tmp));
}

static int		fn_memory(register t_main_42sh *array)
{
	register size_t		tempos;

	array->in.in_first = ft_42sh_list_in_create(array, BUFFER_READ_42SH);
	if ((tempos = fn_pwd(&array->pwd.path)) == 0)//Получаем текущий путь из какого стартуем свою работу
		return (0);
	array->pwd.path_litter = tempos;
	if ((tempos = fn_pwd(&array->pwd.prev_path)) == 0)//Получаем текущий путь из какого стартуем свою работу
		return (0);
	array->pwd.prev_path_litter = tempos;
	return (1);
}

void			ft_42sh_init(register t_main_42sh *array)
{
	array->msg.pre_msg = PRE_MSG_42SH;
	array->msg.pre_msg_litter = PRE_MSG_LITTER_42SH;
	array->msg.pre_msg_sp = MSG_SP_42SH;
	if (tcgetattr(STDOUT_FILENO, &array->tty_change) != 0)//Получим состояние терминала
	{
		write(STDERR_FILENO, E_CANON_42SH, LEN_(E_CANON_42SH));
		exit(E_CODE_42SH);
	}
	array->tty = array->tty_change;//сохраняем для последующего востановления
	array->tty_change.c_lflag &= ~(ICANON | ECHO);//переключаем в не конанический режим //Запрещаем выводить символы на экран ISIG
	array->tty_change.c_cc[VMIN] = 1;//Покуда не считает хоть один файл read не заканчивает свою работу
	array->tty_change.c_cc[VTIME] = 0;//Отменяем ограничения по времени на ожидание
	if (tcsetattr(STDOUT_FILENO, TCSANOW, &array->tty_change) != 0)//Изменили состояние терминала
	{
		write(STDERR_FILENO, E_CANON_42SH, LEN_(E_CANON_42SH));
		exit(E_CODE_42SH);
	}
	ioctl(STDIN_FILENO, TIOCGWINSZ, &array->ws);//Получим размер терминала
	array->out.b = &array->buff_out[0];//Иницилизируем структуру для вывода нескольких команд
	array->out.max = BUFFER_OUT_42SH;
	array->out.fd = STDOUT_FILENO;
	array->err.b = &array->buff_err[0];//Иницилизируем структуру для вывода нескольких команд
	array->err.max = BUFFER_ERR_42SH;
	array->err.fd = STDERR_FILENO;
	array->pr.fd_in = dup(STDIN_FILENO);
	array->pr.fd_out = dup(STDOUT_FILENO);
	array->pr.fd_err = dup(STDERR_FILENO);
	if (fn_memory(array) == 0)
		ft_42sh_exit(E_MEM_CODE_42SH);
}
