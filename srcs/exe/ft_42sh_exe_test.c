/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_exe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static size_t	fn_not_find(register t_main_42sh *array,
register char *cmd, register size_t n, char *str)
{
	ft_42sh_dsp_err_msg_add_n(array, str, cmd, n);
	return (0);
}

static int		fn_test_exe(register t_string *root, register char *cmd,
register size_t n, register char **lp_arg)
{
	register size_t			tempos;
	t_string				ret;
	struct stat				st;

	ft_42sh_parsing_path(&ret, root, cmd, n);
	if (lstat(ret.buff, &st) == 0)
	{
		tempos = st.st_mode;//По правильному надо бы учитывать вазможность ссылак и уже по исходному файлу определять исполняемый он или нет
		if ((tempos & S_IFMT) == S_IFREG &&
		((tempos & S_IXUSR) | (tempos & S_IXGRP) | (tempos & S_IXOTH) != 0))
		{
			lp_arg[0] = ret.buff;
			return (1);
		}
		else
		{
			free(ret.buff);
			return (2);
		}
	}
	free(ret.buff);
	return (0);
}

size_t			ft_42sh_exe_test(register t_main_42sh *array,
register char **lp_arg, register char *cmd, register size_t n)
{
	register char			**spl;
	register size_t			tempos;
	register uint_fast8_t	b_test;
	t_string				root;

	if ((b_test = fn_test_exe(&array->pwd.path, cmd, n, lp_arg)) == 1)
		return (1);
	else if (b_test == 2)
		return (fn_not_find(array, cmd, n, MSG_EXE_DEFINE_42SH));
	if ((spl = array->lp_spl_path) == 0)
		return (fn_not_find(array, cmd, n, MSG_EXE_NOT_CMD_42SH));
	while ((tempos = (size_t)spl++[0]) != 0)
	{
		root.buff = (char *)tempos;
		tempos = ft_strlen((char *)tempos);
		root.len = tempos;
		root.max_len = tempos;
		if ((b_test = fn_test_exe(&root, cmd, n, lp_arg)) == 1)
			return (1);
		else if (b_test == 2)
			return (fn_not_find(array, cmd, n, MSG_EXE_DEFINE_42SH));
	}
	return (fn_not_find(array, cmd, n, MSG_EXE_NOT_CMD_42SH));
}
