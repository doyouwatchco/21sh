/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_exe.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static void		fn_finish(register t_main_42sh *array)
{
	register pid_t				pid;
	int							stat_loc;

	if ((pid = array->pr.b_pid) == 0)
		return ;
	array->pr.b_main_suspend = 1;
	waitpid(pid, &stat_loc, 0);
	array->pr.b_pid = 0;
	array->pr.b_main_suspend = 0;
	tcsetattr(STDOUT_FILENO, TCSANOW, &array->tty_change);//Установим не каонический режим
	if (WIFSIGNALED(stat_loc) && WTERMSIG(stat_loc) == SIGINT)
		ft_write_buffer_str_zero(&array->out, "\n\x1b[J");//Удаляем все справо и ниже каретки
}

static void		fn_exe(register t_main_42sh *array,
register char **lp_arg, register t_jobs_42sh *list)
{
	register char				**env_spl;
	register pid_t				pid;

	if ((env_spl = array->env.lp_spl_env) == 0)
	{
		env_spl = ft_42sh_env_split(array->env.env.first,
		array->env.count_env);
		array->env.lp_spl_env = env_spl;
	}
	if ((pid = fork()) == 0)
	{
		array->pr.b_child = 1;
		tcsetattr(STDOUT_FILENO, TCSANOW, &array->tty);//Установим каонический режим
		ft_42sh_pipe_child(array, list);
		execve(lp_arg[0], lp_arg, env_spl);
		ft_42sh_exit(E_EXE_CODE_42SH);
	}
	else if (pid < 0)
		return (ft_42sh_dsp_err_msg(array, MSG_FOOR_42SH));
	ft_42sh_pipe_redir(array, list);
	list->pid = pid;
	array->pr.b_pid = pid;
}

void			ft_42sh_exe(register t_main_42sh *array)
{
	register t_jobs_42sh		*list;
	register size_t				count;
	register char				**lp_arg;
	register t_fun_42sh			*fun;
	
	list = array->pr.jb.last;
	count = list->count;
	ft_write_buffer(&array->out);//Записываем все команды что на собирались//Что бы в случае редиректа или пайпа не пошли куда не надо и отобразилось как надо
	while (--count > 0)
		list = list->prev;
	while (list != 0)
	{
		lp_arg = list->lp_arg;
		count = list->count_pipes;
		while (count-- > 0)
			if (pipe(list->fds + (count << 1)) != 0)
				ft_42sh_exit(E_PIPE_CODE_42SH);
		if ((fun = list->fun) != 0)
			ft_42sh_pipe_fun(array, list, fun, lp_arg);
		else
			fn_exe(array, lp_arg, list);
		ft_42sh_jobs_fd_close_prev(list);//Закрываем не нужные уже хендлы файлов иначе типа cat будут все ждать конца файла
		list = list->next;
	}
	fn_finish(array);
	ft_42sh_jobs_cut_count(array, array->pr.jb.last);
}
