/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_str_add.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

void					ft_42sh_str_add(register t_main_42sh *array,
register char *str, register char *end, uint_fast8_t correction)
{
	register t_in_42sh			*list;
	register char				*tmp;
	register size_t				count_litter;
	register unsigned char		litter;
	t_add_litter_42sh			in;

	tmp = str;
	count_litter = 0;
	list = array->in.in_current;
	while (str < end)
	{
		count_litter++;
		if (((litter = str++[0])) == '\n')//Если перенос строки то особая обработка
		{
			in.count = str - tmp - correction;
			in.count_litter = count_litter - correction;
			ft_42sh_str(array, list, (void *)tmp, &in);
			in.count = 2;
			in.count_litter = 0;
			ft_42sh_str(array, list, (void *)" \n", &in);
			tmp = str;
			count_litter = 0;
		}
		else if (litter < 0x20)//Все остальные не печатные символы подавляем а так же табуляцию и тому подобное
		{
			in.count = str - tmp - 1;
			in.count_litter = count_litter - 1;
			ft_42sh_str(array, list, (void *)tmp, &in);
			tmp = str;
			count_litter = 0;
		}
		else if ((litter & 0x80) != 0)
			while (((litter = litter << 1) & 0x80) != 0)
				str++;
		
	}
	in.count = str - tmp;
	in.count_litter = count_litter;
	ft_42sh_str(array, list, (void *)tmp, &in);
}
