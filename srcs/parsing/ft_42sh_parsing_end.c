/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_parsing_end.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

size_t			ft_42sh_parsing_end(register t_main_42sh *array,
unsigned char **out, register unsigned char *e, register size_t count)//Догоняет до конца текущей пачки команд и почищет если надо листы с командами
{
	register unsigned char	litter;
	register unsigned char	dquote;
	register unsigned char	*b;

	b = *out;
	dquote = 0;
	while (b < e)
	{
		if ((litter = b[0]) == '\\')
			b++;
		else if (litter == '"' || litter == '\'')
			dquote = (litter == dquote) ? 0 : litter;
		else if ((litter  == ';' || litter == '\n') && dquote == 0)
			break ;
		b++;
	}
	*out = b;
	if (count != 0)
		ft_42sh_jobs_cut_count(array, array->pr.jb.last);
	return (0);
}
