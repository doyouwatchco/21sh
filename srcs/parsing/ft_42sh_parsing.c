/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_parsing.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static t_jobs_42sh	*fn_free(register char *b, register char **lp_arg,
register char **tmp)
{
	free(b);
	if ((b = lp_arg[0]) != 0)
		free(b);
	while ((b = tmp++[0]) != 0)
		free(b);
	free(lp_arg);
	return (0);
}

static t_jobs_42sh	*fn_command(register t_main_42sh *array,
register char **lp_arg, t_string *cmd, size_t count)
{
	register char			*b;
	register char			*name;
	register size_t			n;
	register t_fun_42sh		*fun;

	b = cmd->buff;
	n = cmd->len;
	fun = g_cmd_array;
	while ((name = fun[0].name) != 0)
	{
		if (n == fun[0].count && ft_strncmp(b, name, n) == 0)
			break ;
		fun++;
	}
	if (name != 0)
		;
	else if (ft_42sh_exe_test(array, lp_arg, b, n) != 0)
		fun = 0;
	else
		return (fn_free(b, lp_arg, lp_arg + 1));//Так как первый аргумент указывает всегда путь к исполняемому файлу вызываемой программы
	free(b);
	return (ft_42sh_jobs_create_add_list(array, lp_arg, fun, count));
}

static void			fn_parsing_command_grup(register t_main_42sh *array,
unsigned char *b, register unsigned char *end, register size_t count)
{
	register char			**lp_arg;
	t_string				cmd;//Хранит команду на исполнение после использования нужно освободить буффер строки
	unsigned char			litter;

	while (b < end)
	{
		ft_42sh_parsing_cmd(array, &cmd, &b, end);//Парсим команду какую нужно выполнить
		if ((lp_arg = ft_42sh_parsing_arg(array, &b, end, cmd.len)) != 0)
		{
			if (fn_command(array, lp_arg, &cmd, ft_42sh_pipe_next_count(array, b, end)) == 0)
				count = ft_42sh_parsing_end(array, &b, end, count);
			else if (ft_42sh_pipe_pre(array, &b, end, ++count) == 0)
				count = ft_42sh_parsing_end(array, &b, end, count);
		}
		if (b == end || ((litter = b[0]) == ';' || litter == '\n'))
		{
			if (count != 0)
				ft_42sh_exe(array);
			count = 0;
			b++;
		}
	}
}

void			ft_42sh_parsing(register t_main_42sh *array,
register t_in_42sh *list, register char litter)
{
	register t_in_42sh			*parser;
	register unsigned char		*b;
	register unsigned char		*e;
	register size_t				tempos;

	if ((parser = array->dq.in_dquote) == 0)//Если был включен режим кавычек тогда обработаем полученый лист уже
		parser = list;
	else
		array->dq.in_dquote = 0;
	if (litter != -1)
	{
		b = (unsigned char *)parser->lp_b;
		if ((tempos = array->dq.hrdoc_b_count) != 0)//Если heredoc есть то ищем до его границы
			e = b + tempos;
		else
			e = b + parser->count;
		fn_parsing_command_grup(array, b, e, 0);
	}
	ft_42sh_init_out(array, list);
	ft_42sh_dsp_start(array);
}
