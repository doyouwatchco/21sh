/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_parsing_test.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

unsigned char		ft_42sh_parsing_test_pipe(register unsigned char *b,
register unsigned char *e, register unsigned char litter, unsigned char old)
{
	if (b == e)
		return (0);
	if (litter == '|' || litter == '>'|| litter == '<')
		return (1);
	if (old == 0x20 && ++b < e)
	{
		if (litter >= 0x30 && litter <= 0x39)
		{
			if (((old = b[0]) == '<') || old == '>')
				return (2);
			else if (old == '&' && ++b < e && (((old = b[0]) == '<') || old == '>'))
				return (3);
		}
	}
	return (0);
}

unsigned char		ft_42sh_parsing_test_next(unsigned char **out,
register unsigned char *e, register unsigned char dquote, unsigned char old)
{
	register unsigned char	litter;
	register unsigned char	*b;

	b = *out;
	if (b == e || ((litter = b[0]) == ';' && dquote == 0) || litter < 0x20)
		return (0);
	if (dquote == 0 && ft_42sh_parsing_test_pipe(b, e, litter, old) != 0)
		return (0);
	b++;
	if (litter == 0x20)
	{
		if (dquote == 0)//Все что меньше или равно 0x20 и не экранировано - конец поиска
			litter = 0;
		else if (b < e && b[0] == '\n')
		{
			b++;
			litter = '\n';
		}
	}
	*out = b;
	return (litter);
}

size_t				ft_42sh_parsing_test(register t_main_42sh *array,
unsigned char *b, register unsigned char *end)
{
	register size_t					count;
	register unsigned char			litter;
	register size_t					tempos;

	while (b < end)
	{
		count = ft_42sh_replase_cmd_count(array, &b, end, 0);
		ft_42sh_parsing_arg_empty(array, &b, end);
		if ((tempos = ft_42sh_pipe_test(array, &b, end, count)) == 0)
			break ;
		if (b < end && ((litter = b[0]) == ';' || litter == '\n'))
		{
			if (tempos == PIPE_LEFT2_42SH && litter == '\n')
				break ;
			count = 0;
			b++;
		}
	}
	return (tempos);
}
