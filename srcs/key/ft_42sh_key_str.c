/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_key_str.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

void			ft_42sh_key_str_delete(register t_main_42sh *array,
register t_in_42sh *list)
{
	register unsigned char		*b;
	register t_slesh_42sh		**spl;

	if (list->count == 0)//если пустой ничего не делаем
		return ;
	ft_42sh_list_in_dup(list);
	if ((b = array->slc.b) == 0)
	{
		if ((spl = list->spl_slesh) != 0)
		{
			b = (void *)list->lp_current;
			spl = &spl[list->slesh_current];
			if (spl[0]->count == 0 || (list->lp_b != (char *)b && b[-1] != '\n'))
			{
				ft_42sh_key_home(array, list);
				spl = &list->spl_slesh[list->slesh_current];
			}
			return (ft_42sh_str_delete(array, list, list->lp_current +
			spl[0]->count + (list->slesh_current != list->slesh_max ? 2 : 0),
			spl[0]->count_litter));
		}
		ft_42sh_key_home(array, list);
		ft_write_buffer_str_zero(&array->out, "\x1b[J");//Удаляем все справо и ниже каретки
		return (ft_42sh_list_in_default(array, list));

	}
	if (list->lp_current == (char *)b)
		b = array->slc.e;
	ft_42sh_str_delete(array, list, (void *)b, array->slc.count_litter);
	array->slc.b = 0;
}

static size_t	fn_slesh_pre(register t_in_42sh *list,
register t_slesh_42sh **spl_slesh)
{
	register t_slesh_42sh		**spl_tmp;
	register size_t				count;

	spl_tmp = list->spl_slesh;
	count = 0;
	while (spl_tmp < spl_slesh)//Подсчитаем количество байт
		count += spl_tmp++[0]->count + 2;
	count += spl_slesh[0]->count;
	return (spl_slesh[0]->count - (count - (list->lp_current - list->lp_b)));
}

void			ft_42sh_key_str_copy(register t_main_42sh *array,
register t_in_42sh *list)
{
	register unsigned char		*b;
	register void				*out;
	register t_slesh_42sh		**spl;
	register size_t				count;

	if ((b = array->slc.lp_clipboard) != 0)
	{
		array->slc.lp_clipboard = 0;
		free(b);
	}
	if (list->count == 0)//если пустой ничего не делаем
		return ;
	if ((b = array->slc.b) == 0)
	{
		if ((spl = list->spl_slesh) != 0)
		{
			b = (void *)list->lp_current;
			spl = &spl[list->slesh_current];
			if ((count = spl[0]->count) == 0)
				return (ft_42sh_dsp_clear_select(array, list));
			if ((list->lp_b != (char *)b && b[-1] != '\n'))
				b -= fn_slesh_pre(list, spl);
		}
		else
		{
			b = (void *)list->lp_b;
			count = list->count;
		}
	}
	else
		count = array->slc.e - b;
	if ((out = malloc(count)) == 0)//Выделяем буфер
		ft_42sh_exit(E_MEM_CODE_42SH);
	ft_memcpy(out, b, count);
	ft_42sh_dsp_clear_select(array, list);
	array->slc.lp_clipboard = out;
	array->slc.clipboard_count = count;
}

void			ft_42sh_key_str_paste(register t_main_42sh *array,
register t_in_42sh *list)
{
	register char			*b;

	if ((b = array->slc.lp_clipboard) != 0)
	{
		if (array->slc.b != 0)
			ft_42sh_key_str_delete(array, list);
		return (ft_42sh_str_add(array, b, b + array->slc.clipboard_count, 2));
	}
	if (list->count == 0)
		return ;
	ft_42sh_key_str_copy(array, list);
	b = array->slc.lp_clipboard;
	ft_42sh_str_add(array, b, b + array->slc.clipboard_count, 2);
}

void			ft_42sh_key_str_cut(register t_main_42sh *array,
register t_in_42sh *list)
{
	t_select_42sh			slc;
	register unsigned char	*b;

	if (list->count == 0)
		return ;
	slc.b = array->slc.b;
	slc.e = array->slc.e;
	slc.count_litter = array->slc.count_litter;
	ft_42sh_key_str_copy(array, list);
	b = slc.b;
	array->slc.b = b;
	array->slc.e = slc.e;
	array->slc.count_litter = slc.count_litter;
	ft_42sh_key_str_delete(array, list);
}
