/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_key_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static int				fn_test_all_cmd(register unsigned char *lp_b,
register unsigned char *lp_current, register unsigned char *end)//Проверим  подходит ли условия для показа всех команд - кроме ';', и непечатных символов нечего нет
{
	register unsigned char		*tmp;
	register unsigned char		litter;

	tmp = lp_current - 1;
	while (lp_current < end &&
	(litter = lp_current[0]) <= 0x20 && litter != ';' && litter != '|')
		lp_current++;
	if (lp_current < end && litter != ';' && litter != '|')
		return (0);
	while (tmp >= lp_b && (litter = tmp[0]) <= 0x20 && litter != ';' && litter != '|')
		tmp--;
	if (tmp >= lp_b && litter != ';' && litter != '|')
		return (0);
	return (1);
}

static int				fn_test_env_path(register unsigned char *b,
register unsigned char *e)//Проверим может это дополнение переменной среды или путь к папке
{
	register unsigned char		litter;
	unsigned char				*tmp;

	if ((litter = b[0]) == '$' || litter == '.')
		return (0);
	tmp = b;
	while (b < e)
	{
		if (b[0] == '/' && tmp != b && b[-1] != '\\')
			return (0);
		b++;
	}
	return (1);
}

static int				fn_test_cmd(register t_main_42sh *array,
register t_in_42sh *list, register unsigned char *lp_b,
register unsigned char *end)//Проверим  подходит ли условия для показа некоторых команд
{
	register unsigned char		*lp_current;
	register unsigned char		*tmp;
	register unsigned char		*start;
	register unsigned char		litter;

	lp_current = (void *) list->lp_current;
	tmp = lp_current - 1;
	while (tmp >= lp_b && (litter = tmp[0]) > 0x20 && litter != ';' && litter != '|')
		tmp--;
	start = tmp;
	if (start < lp_b || litter == ';' || litter <= 0x20)
		start++;
	while (tmp >= lp_b && (litter = tmp[0]) <= 0x20 && litter != ';' && litter != '|')
		tmp--;
	if (tmp >= lp_b && litter != ';' && litter != '|')
		return (0);
	while (lp_current < end && (litter = lp_current[0]) > 0x20 && litter != ';' && litter != '|')
		lp_current++;
	if (fn_test_env_path(start, lp_current) == 0)
		return (0);
	ft_42sh_auto_cmd(array, list, start, lp_current - start);
	return (1);
}

void					ft_42sh_key_tab(register t_main_42sh *array,
register t_in_42sh *list)
{
	register unsigned char		*lp_b;
	register unsigned char		*end;

	array->lp_auto = &array->auto_;
	lp_b = (unsigned char *)list->lp_b;
	end = lp_b + list->count;
	if (fn_test_all_cmd(lp_b, (void *) list->lp_current, end) != 0)
		return (ft_42sh_auto_cmd(array, list, 0, 0));
	if (fn_test_cmd(array, list, lp_b, end) != 0)
		return ;
	array->lp_auto = &array->auto_file;
	ft_42sh_auto_comp(array, list);
}
