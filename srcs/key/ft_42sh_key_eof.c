/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_key_eof.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

void			ft_42sh_key_eof(register t_main_42sh *array,
register t_in_42sh *list)
{
	register size_t				count;

	if ((count = list-> count) == 0)
	{
		ft_write_buffer_str_zero(&array->out, "\n");
		ft_42sh_cm_exit(array, 0);//При ctr + D - выходим так как можем получить данную команду если не запущен другой процесс
	}
	if (array->dq.dquote != 0 || list->lp_b + count == list->lp_current)
		return (ft_write_buffer_str_zero(&array->out, "\x7"));//Звуки
	ft_42sh_key_delete(array, list);
}
