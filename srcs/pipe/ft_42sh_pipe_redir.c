/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_pipe_redir.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

// static void		fn_while(register t_jobs_42sh *list,register t_pipe_42sh *pipe,
// register size_t n, register size_t b_count)
// {
// }

void			ft_42sh_pipe_redir(register t_main_42sh *array,
register t_jobs_42sh *list)
{
	register t_pipe_42sh		*pipe;
	register size_t				b_count;
	register size_t				count;
	register size_t				n;
	register char				*b;

	b_count = list->count;
	pipe = list->pipe;
	if ((n = list->n) == 0 || pipe[0].type == '|')
		return ;
	//fn_while(list, pipe, n, b_count);
	b = (void *) &array->litter;
	count = read(list->fds[PIPE_READ_42SH], b, sizeof(array->litter));
	(void)array;
}
