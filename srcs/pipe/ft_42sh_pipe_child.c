/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_pipe_child.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static void		fn_last(register t_jobs_42sh *list,
register size_t b_count)
{
	b_count = ((b_count & 1) == 0) ? 0 : 2;
	dup2(list->fds[b_count + PIPE_READ_42SH], STDIN_FILENO);
	close(list->fds[b_count + PIPE_WRITE_42SH]);//Закрываем не нужные уже хендлы файлов иначе типа cat будут все ждать конца файла
}

static void		fn_central(register t_jobs_42sh *list,
register t_jobs_42sh *prev, register size_t b_count)
{
	b_count = ((b_count & 1) == 0) ? 0 : 2;
	dup2(prev->fds[b_count + PIPE_READ_42SH], STDIN_FILENO);
	close(prev->fds[b_count + PIPE_WRITE_42SH]);//Закрываем не нужные уже хендлы файлов иначе типа cat будут все ждать конца файла
	b_count = (b_count == 0) ? 2 : 0;
	dup2(list->fds[b_count + PIPE_WRITE_42SH], STDOUT_FILENO);
	close(list->fds[b_count + PIPE_READ_42SH]);//Закрываем не нужные уже хендлы файлов иначе типа cat будут все ждать конца файла
}

static void		fn_first(register t_jobs_42sh *list,
register t_jobs_42sh *prev, register size_t b_count)
{
	if (b_count > 1)
		return (fn_central(list, prev, b_count));
	dup2(list->fds[PIPE_WRITE_42SH], STDOUT_FILENO);
	close(list->fds[PIPE_READ_42SH]);//Закрываем не нужные уже хендлы файлов иначе типа cat будут все ждать конца файла
}

static void		fn_redirect(register t_jobs_42sh *list,
register t_jobs_42sh *prev, register size_t b_count)
{
	if (list->pipe[list->n - 1].type == '|' || (b_count > 1 && prev->pipe[prev->n - 1].type == '|'))
		b_count = ((b_count & 1) == 0) ? 2 : 0;
	else
		b_count = 0;
	dup2(list->fds[b_count + PIPE_WRITE_42SH], STDOUT_FILENO);
	close(list->fds[b_count + PIPE_READ_42SH]);//Закрываем не нужные уже хендлы файлов иначе типа cat будут все ждать конца файла
}

void			ft_42sh_pipe_child(register t_main_42sh *array,//ls | cat -e | cat -b | cat -n
register t_jobs_42sh *list)//fd ->write | read<-fd fd2->write | read<-fd2 fd->write | read<-fd
{
	register size_t				b_count;
	register t_jobs_42sh		*prev;

	b_count = list->count;
	prev = list->prev;
	if (list->n != 0)
	{
		if (list->pipe[0].type == '|')
			fn_first(list, prev, b_count);
		else
			fn_redirect(list, prev,b_count);
	}
	else if (list->count != 1 && prev->n != 0 && prev->pipe[prev->n - 1].type == '|')
		fn_last(prev, b_count);
	(void)array;
}
