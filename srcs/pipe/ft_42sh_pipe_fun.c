/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_pipe_fun.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static int		fn_last(register t_jobs_42sh *list,
register size_t b_count)
{
	return (STDIN_FILENO);
	(void)b_count;
	(void)list;
}

static int		fn_central(register t_jobs_42sh *list,
register t_jobs_42sh *prev, register size_t b_count)
{
	return (list->fds[((b_count == 0) ? 2 : 0) + PIPE_WRITE_42SH]);
	(void)prev;
}

static int		fn_first(register t_jobs_42sh *list,
register t_jobs_42sh *prev, register size_t b_count)
{
	if (b_count > 1)
		return (fn_central(list, prev, b_count));
	return (list->fds[PIPE_WRITE_42SH]);
	return (0);
}

void			ft_42sh_pipe_fun(register t_main_42sh *array,
register t_jobs_42sh *list, register t_fun_42sh *fun, register char **lp_arg)//fd ->write | read<-fd fd2->write | read<-fd2 fd->write | read<-fd
{
	register size_t				b_count;
	register t_jobs_42sh		*prev;
	int							fd;

	fd = array->out.fd;
	b_count = list->count;
	prev = list->prev;
	if (list->n != 0 && list->pipe[0].type == '|')
		array->out.fd = fn_first(list, prev, b_count);
	else if (list->count != 1 && prev->n != 0 && prev->pipe[prev->n - 1].type == '|')
		array->out.fd = fn_last(prev, b_count);
	if (array->out.fd == fd)
		return (fun[0].f(array, lp_arg + 1));
	ft_write_buffer(&array->out);//Записываем все команды что на собирались//Что бы в случае редиректа или пайпа не пошли куда не надо и отобразилось как надо
	fun[0].f(array, lp_arg + 1);
	ft_write_buffer(&array->out);//Записываем все команды что на собирались//Что бы в случае редиректа или пайпа не пошли куда не надо и отобразилось как надо
	array->out.fd = fd;
}
