/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_pipe_pre.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static char		fn_open(register t_main_42sh *array,
register t_pipe_42sh *pipe, register unsigned char *dest)
{
	register int			fd;

	fd = O_CREAT | O_RDWR | (pipe->type == PIPE_RIGHT2_42SH ? O_APPEND : 0);
	fd = open((char *)dest, fd, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if (fd == -1)
	{
		ft_42sh_dsp_err_msg_add(array, MSG_EXE_DEFINE_42SH, (char *)dest);
		free(dest);
		return (0);
	}
	free(dest);
	pipe->fd_out = fd;
	return (1);
}

static char		fn_number(register t_main_42sh *array,
register t_pipe_42sh *pipe, register unsigned char *dest,
register unsigned char *end)
{
	register unsigned char			*b;
	register unsigned char			litter;
	register size_t					count;

	b = dest;
	while (b < end && (litter = b[0]) >= 0x30 && litter <= 0x39)
		b++;
	if (b != end)
	{
		free(dest);
		ft_42sh_dsp_err_msg(array, MSG_PIPE_NUMBER_42SH);
		return (0);
	}
	b = dest;
	while (b < end && b[0] == 0x30)
		b++;
	count = end - b;
	if (count > 2 ||(litter = b[0] - 0x30) > PIPE_MAX_FD_42SH)
	{
		ft_42sh_dsp_err_msg_add_n(array, MSG_PIPE_BAD_FD_42SH, (char *)b, count);
		free(dest);
		return (0);
	}
	pipe->fd_out = litter;
	return (1);
}

static size_t		fn_set_heredok(register t_main_42sh *array,
register t_pipe_42sh *pipe, unsigned char **out, register unsigned char *e)
{
	register size_t					count;
	register unsigned char			*b;

	b = *out;
	count = ft_42sh_replase_cmd_count(array, out, e, 0);
	if (pipe->type != PIPE_LEFT2_42SH)
		return (count);
	b = ((t_jobs_42sh *)array->pr.jb.last)->lp_next_heredoc;
	return (count);
}

static char		fn_set_add(register t_main_42sh *array,
register t_pipe_42sh *pipe, unsigned char **out, register unsigned char *end)
{
	register unsigned char			*b;
	register unsigned char			*dest;
	register unsigned char			litter;
	register size_t					count;

	b = *out;
	count = fn_set_heredok(array, pipe, out, end);
	if ((litter = b[0]) == '&')
	{
		b++;
		if (b[0] == '-' && count == 2)
			return (pipe->b_flag |= PIPE_FLAG_CLOSE_42SH);
		count--;
	}
	if ((dest = malloc(count + 1)) == 0)
		ft_42sh_exit(E_MEM_CODE_42SH);
	dest[count] = 0;
	ft_42sh_replase_cmd(array, dest, b, end);
	if (litter == '&')
		return (fn_number(array, pipe, dest, dest + count));
	if (pipe->type == PIPE_RIGHT_42SH || pipe->type == PIPE_RIGHT2_42SH)
		return (fn_open(array, pipe, dest));
	else
		return (1);
}

static char		fn_set(register t_main_42sh *array,
register t_pipe_42sh *pipe, unsigned char **out, register unsigned char *end)
{
	register unsigned char			*b;
	register unsigned char			litter;

	b = *out;
	if ((litter = b[0]) == '|')
	{
		*out = *out + 1;
		return (pipe->type = PIPE_42SH);
	}
	if (litter == 0x31 || litter == 0x32)
	{
		pipe->fd_in = litter - 0x30;
		b++;
	}
	if ((litter = b++[0]) == '&')
	{
		litter = b++[0];
		pipe->b_flag |= PIPE_FLAG_AND_42SH;
	}
	pipe->type = litter;
	if (litter == b[0])
	{
		b++;
		pipe->type += 1;
	}
	*out = b;
	if (fn_set_add(array, pipe, out, end) == 0)
		return (0);
	return (pipe->type);
}

size_t			ft_42sh_pipe_pre(register t_main_42sh *array,
unsigned char **out, register unsigned char *end, register size_t count)
{
	register t_jobs_42sh			*list;
	register t_pipe_42sh			*pipe;
	register unsigned char			*b;
	register unsigned char			litter;

	list = array->pr.jb.last;
	list->count = count;
	if (list->n == 0)
		return (1);
	list->lp_next_heredoc = end;
	pipe = list->pipe;
	b = *out;
	while (b < end && (litter = b[0]) != ';' && litter != '\n')
	{
		if (ft_42sh_parsing_test_pipe(b, end, litter, 0x20) == 0)
		{
			while (0xFF)
			{
				ft_42sh_replase_arg_count(array, out, end, 0);
				if (*out == b)
					break;
				b = *out;
			}
		}
		if ((litter = fn_set(array, pipe++, out, end)) == 0)
			return (0);
		b = *out;
		if (litter == PIPE_42SH)
			break ;
		
	}
	pipe = list->pipe;
	if ((litter = pipe[0].type) != '|' && pipe[list->n - 1].type == '|')
		list->count_pipes = 3;
	else if (list->count > 1 && litter == '|')
		list->count_pipes = 2;
	else
		list->count_pipes = 1;
	return (1);
}
