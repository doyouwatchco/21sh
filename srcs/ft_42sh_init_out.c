/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_init_out.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static void		fn_dup_list(register t_main_42sh *array,
register t_in_42sh *list)//Сжелаем копию и сместим в предпоследнюю позицию
{
	register t_in_42sh	*new;
	register char		*lp_b;

	if ((new = malloc(sizeof(t_in_42sh))) == 0)//Выделяем буфер для структуры
		ft_42sh_exit(E_MEM_CODE_42SH);
	ft_memcpy(new, list, sizeof(t_in_42sh));//Переносим структуру
	if ((lp_b = malloc(list->max)) == 0)//Выделяем буфер для даннных
		ft_42sh_exit(E_MEM_CODE_42SH);
	ft_memcpy(lp_b, list->lp_b, list->max);//Переносим данные
	new->lp_b = lp_b;
	new->lp_current = lp_b + (list->lp_current - list->lp_b);//Каректируем
	list->spl_slesh = ft_42s_dq_split_dup(list->spl_slesh, list->slesh_max + 1);
	new->max_dup = 0;//В новом листе копия старай строки не нужна само собой
	array->in.in_count++;
	list = array->in.in_last;
	new->next = list;
	lp_b = (char *)list->prev;//Так как тут будим только в том случае когда еть не только последний лист но у другие поэтому не проверяем возможность нуля у prev
	list->prev = new;
	((t_in_42sh *)lp_b)->next = new;
	new->prev = (t_in_42sh *)lp_b;
}

void			ft_42sh_init_out(register t_main_42sh *array,
register t_in_42sh *list)
{
	register size_t			count;
	register unsigned char	*tempos;

	tempos = (unsigned char *)list->lp_b;//unsigned char - чтобы не пропустить utf-8 символы
	count = list->count;
	while (count != 0 && tempos++[0] <= 0x20)
		count--;
	if (count != 0 || list->spl_slesh != 0)//Если есть какие либо буквы кроме пробела и не печатных то либо копию создадим или новый конечный лист для получения букв
	{
		if (list->next == 0)
			ft_42sh_list_in_create(array, BUFFER_READ_42SH);
		else
			fn_dup_list(array, list);
	}
	ft_42sh_list_in_last(array);//Востановим значение конечного листа на дефолтные и заодно сделаем его текущим
	ft_42sh_list_in_dup_restore(array);
	ft_42sh_list_in_limit(array);
}
