/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_jobs_list.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

void			ft_42sh_jobs_free_list(register t_jobs_42sh *list)
{
	register char		**lp_arg;
	register char		**lp_tmp;
	register char		*tmp;

	lp_arg = list->lp_arg;
	free(list);
	if (lp_arg == 0)
		return ;
	lp_tmp = lp_arg + 1;
	if ((tmp = lp_arg[0]) != 0)
		free(tmp);
	while ((tmp = lp_tmp++[0]) != 0)
		free(tmp);
	free(lp_arg);
}

void			ft_42sh_jobs_cut_count(register t_main_42sh *array,
register t_jobs_42sh *list)
{
	register t_jobs_42sh		*tmp;
	register t_jobs_42sh		*last;
	register size_t				count;

	last = list->next;
	count = list->count;
	while (count-- > 0)
	{
		tmp = list;
		list= list->prev;
		ft_42sh_jobs_free_list(tmp);
	}
	if (last == 0 && list == 0)
		array->pr.jb.first = 0;
	else if (list == 0)
	{
		array->pr.jb.first = last;
		last->prev = 0;
	}
	else if (last == 0)
	{
		array->pr.jb.last = list;
		list->next = 0;
	}
	else
	{
		list->next = last;
		last->prev = list;
	}
}

void			ft_42sh_jobs_fd_close_prev(register t_jobs_42sh *list)
{
	register size_t				count;
	register int				*fds;

	if (list->count == 1)
		return ;
	list = list->prev;
	count = list->count_pipes << 1;
	fds = list->fds;
	while (count-- > 0)
		close(fds++[0]);
}

t_jobs_42sh		*ft_42sh_jobs_create_add_list(register t_main_42sh *array,
register char **lp_arg, register t_fun_42sh *fun, register size_t count)
{
	register t_jobs_42sh		*out;
	register t_jobs_42sh		*last;
	register size_t				tempos;

	tempos = sizeof(t_jobs_42sh) + (count * sizeof(t_pipe_42sh));
	if ((out = malloc(tempos)) == 0)
		ft_42sh_exit(E_MEM_CODE_42SH);
	ft_memset(out, 0, tempos);
	if (array->pr.jb.first != 0)
	{
		last = array->pr.jb.last;
		last->next = out;
		out->prev = last;
	}
	else
		array->pr.jb.first = out;
	array->pr.jb.last = out;
	out->lp_arg = lp_arg;
	out->fun = fun;
	out->n = count;
	return (out);
}
