/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_dq_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static char	fn_test_slh(register char *b, register size_t n)//Контролирует есть ли перенос строки '\' и включает режим dquote
{
	register char		*lp_current;
	register size_t		tempos;

	lp_current = b + n - 1;//вычисляем  конец строки
	tempos = 0;
	while (n-- != 0 && lp_current--[0] == '\\')
		tempos++;
	if ((tempos & 0x1) == 0)
		return (0);
	return ('\\');
}

static char	fn_test_dq(register char *b, register size_t n)
{
	register char		litter;
	register size_t		tempos;

	tempos = 0;
	while (tempos < n)
	{
		litter = b[tempos++];
		if (litter == '\\' && tempos < n)
			tempos += tempos + 1 < n &&  b[tempos + 1] == '\n' ? 2 : 1;
		else if ((litter == '"' || litter == '\''))
		{
			while (tempos < n)
			{
				if (litter == b[tempos++] && b[tempos - 2] != '\\')
				{
					litter = 0;
					break ;
				}
			}
			if (litter != 0)
				return (litter);
		}
	}
	return (fn_test_slh(b, n));
}

char		ft_42sh_dq_test(register t_main_42sh *array,
register char *b, register size_t n, register char litter)
{
	register size_t		tmp;

	if (litter == '\\')
		return (fn_test_dq(b, n));
	if (n == 0)
		return (litter);
	if (litter == b[0])
		return (fn_test_dq(b + 1, n - 1));
	if (litter == PIPE_LEFT2_42SH)
	{
		tmp = (size_t)array->dq.hrdoc_cmp_lp;
		if (array->dq.hrdoc_cmp_count != n || ft_strncmp(b, (char *)tmp, n) != 0)//проверяем heredoc завершен или нет
			return (litter);
		else
		{
			free((void *)tmp);
			return ((char)(array->dq.hrdoc_cmp_lp = 0));
		}
	}
	tmp = 0;
	while (tmp < n)
		if (litter == b[tmp++] && b[tmp - 2] != '\\')
			return (fn_test_dq(b + tmp, n - tmp));
	return (litter);
}

static char	fn_hrdoc(register t_main_42sh *array,
register t_in_42sh *list, register void *b, register void *current)
{
	register char		litter;

	if ((litter = ft_42sh_parsing_test(array, b, current)) == 0)
		return (-1);
	if (litter == PIPE_LEFT2_42SH)
	{
		array->dq.hrdoc_b_count = current - b;
		return (ft_42sh_dq_heredoc_test(array, list, b, current));
	}
	return (0);
}

char		ft_42sh_dq_test_start(register t_main_42sh *array,
register t_in_42sh *list, register char *b, register char *e)
{
	register char		*start;
	register char		litter;

	start = b;
	while (b < e)
	{
		if ((litter = b++[0]) == '\\' && b < e)
			b += b + 1 < e && b[1] == '\n' ? 2 : 1;
		else if ((litter == '"' || litter == '\''))
		{
			while (b < e && (litter != b[0] || b[-1] == '\\'))
				b++;
			if (b++ == e)
				return (litter);
		}
		else if (litter == '\n')
		{
			if ((litter = ft_42sh_parsing_test(array, (void *)start, (void *)b)) == 0)
				return (-1);
			if (litter == PIPE_LEFT2_42SH)
			{
				array->dq.hrdoc_b_count = b - start;
				return (ft_42sh_dq_heredoc_test(array, list, (void *)start, (void *)b));
			}
		}
	}
	if ((litter = fn_test_slh(start, list->count)) == 0)
		return (fn_hrdoc(array, list, start, b));
	array->dq.hrdoc_b_count = b - start;
	return (litter);
}
