/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_dq_add.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

void		ft_42sh_dq_cut(register t_main_42sh *array,
register t_in_42sh *list)
{
	register t_in_42sh	*last;
	register t_in_42sh	*prev;

	ft_42sh_init_out(array, list);//Добавим пустой лист для dquote
	last = array->in.in_last;
	prev = last->prev;
	if (prev->spl_slesh == 0)
		prev->slesh_max = 0; 
	array->dq.in_dquote = prev;
	prev->count_litter -= array->msg.pre_msg_litter;//Удаляем количество стартовых букв - потом могут измениться - что бы не было проблемм
	prev = prev->prev;
	if (prev != 0)
		prev->next = last;
	else
		array->in.in_first = last;//На случай если только два листа присутсвуют
	last->prev = prev;
}

void		ft_42sh_dq_paste(register t_main_42sh *array)
{
	register t_in_42sh	*last;
	register t_in_42sh	*prev;
	register t_in_42sh	*dquote;

	dquote = array->dq.in_dquote;
	last = array->in.in_last;
	dquote->lp_current = dquote->lp_b;
	dquote->count_litter_current = array->msg.pre_msg_litter;//Востановим количество стартовых букв
	dquote->count_litter += array->msg.pre_msg_litter;//Востановим количество стартовых букв
	prev = last->prev;
	if (prev != 0)
		prev->next = dquote;
	else
		array->in.in_first = dquote;//На случай если только два листа присутсвуют - dquote  и last
	last->prev = dquote;
	dquote->next = last;
	dquote->prev = prev;
	ft_42sh_list_in_last(array);//Востановим значение конечного листа на дефолтные и заодно сделаем его текущим
}
