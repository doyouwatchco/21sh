/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_dq.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

void			ft_42sh_dq(register t_main_42sh *array,
register t_in_42sh *list)
{
	register void				*tmp;
	register t_in_42sh			*parser;
	register char				litter;

	ft_42sh_str_full_end(array, list);//Сначала сместим каретку в саммый коне прежде чем будем выводить что либо
	ft_write_buffer_str_zero(&array->out, "\n\x1b[J");//Стартуем стандартную начальное отображение прежде чем выводить что либо почистим экран сначала
	if ((litter = array->dq.dquote) != 0)
	{
		parser = array->dq.in_dquote;
		ft_42sh_dq_combi_date(array, parser, list);//Соединим старые с новыми данными
		if ((litter = ft_42sh_dq_test(array, list->lp_b, list->count, litter)) == 0)
		{
			tmp = parser->lp_b;
			parser->slesh_current = parser->slesh_max;
			ft_42s_dq_split(parser, parser->slesh_max);//образования slesh
			if (array->dq.hrdoc_b_count == 0)
			{
				array->dq.hrdoc_b_count = parser->count;
				array->dq.hrdoc_slesh = parser->slesh_max;
			}
			if ((litter = ft_42sh_dq_heredoc_test(array, parser, tmp, tmp + array->dq.hrdoc_b_count)) == 0)
				ft_42sh_dq_paste(array);//Вставиим лист в котором был включен данный режим
			else
			{
				ft_strsplit_free((char **)parser->spl_slesh);
				parser->spl_slesh = 0;
			}
		}
	}
	else
	{
		array->dq.hrdoc_b_count = 0;
		tmp = list->lp_b;
		if ((litter = ft_42sh_dq_test_start(array, list, tmp, tmp + list->count)) != 0 && litter != -1)
			ft_42sh_dq_cut(array, list);//Вырежим лист в котором был включен данный режим
	} 
	ft_42sh_msg_change(array, ((litter == -1) ? 0 : litter), array->dq.dquote);//Изменим стартовый текс и изменим длину стартовых букв во всех листах
	if (litter == 0 || litter == -1)
		return (ft_42sh_parsing(array, list, litter));//С данного режима вышли - запускаем парсинг команд
	ft_42sh_list_in_last(array);//Востановим значение конечного листа на дефолтные и заодно сделаем его текущим
	ft_write_buffer_str_zero(&array->out, array->msg.pre_msg);
	ft_write_buffer_str_zero(&array->out, " \x1b[1D");//Для того что бы перевод строки авто был вставляем лишний пробел и потом смещаем вправо сразу что бы не было видно
}
