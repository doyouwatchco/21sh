/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_dq_heredoc_test.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static size_t	fn_test(register t_main_42sh *array, register t_in_42sh *list,
register void *cmp, register size_t n)
{
	register t_slesh_42sh		**spl;
	register t_slesh_42sh		*spl_lesh;
	register size_t				count;
	register size_t				tempos;
	register void				*b;

	if ((spl = list->spl_slesh) == 0)
		return (1);
	tempos = 0;
	count = array->dq.hrdoc_b_count;
	while ((spl_lesh = spl++[0]) != 0)
		if ((tempos += spl_lesh->count + 2) >= count)
			break ;
	if (spl_lesh == 0)
		return (1);
	b = &list->lp_b[tempos];
	while ((spl_lesh = spl++[0]) != 0)
	{
		if ((tempos = spl_lesh->count) == n && ft_strncmp(b, cmp, n) == 0)
			return (0);
		else
			b += tempos + 2;
		
	}
	return (1);
}

char			ft_42sh_dq_heredoc_test(register t_main_42sh *array,
register t_in_42sh *list, unsigned char *b, register unsigned char *end)
{
	register unsigned char			litter;
	register size_t					n;
	register unsigned char			*tmp;
	register void					*cmp;

	while (b < end)
	{
		ft_42sh_replase_cmd_count(array, &b, end, 0);
		ft_42sh_parsing_arg_empty(array, &b, end);
		while (ft_42sh_pipe_test(array, &b, end, -1) == PIPE_LEFT2_42SH)
		{
			tmp = ++b;
			n = ft_42sh_replase_cmd_count(array, &b, end, 0);
			if ((cmp = malloc(n)) == 0)
				ft_42sh_exit(E_MEM_CODE_42SH);
			ft_42sh_replase_cmd(array, cmp, tmp, end);
			if (fn_test(array, list, cmp, n) != 0)
			{
				array->dq.hrdoc_cmp_lp = cmp;
				array->dq.hrdoc_cmp_count = n;
				return (PIPE_LEFT2_42SH);
			}
			free(cmp);
		}
		if (b < end && ((litter = b[0]) == ';' || litter == '\n'))
			b++;
	}
	return (0);
}
