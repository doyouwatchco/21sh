#include "ft_42sh.h"

// void	ft_42sh_add_symb(char *d_name, char *sym, register t_main_42sh *array)//добавляем нужный символ к концу имени файла
// {
// 	register char *str;

// 	if (!(str = ft_strjoin(d_name, sym)))
// 		ft_42sh_exit(E_MEM_CODE_42SH);
// 	ft_42sh_auto_add_list(array, str, 0);
// 	free(str);
// }

size_t	ft_42sh_type_file(char *d_name, register t_main_42sh *array)//определяем тип файла
{
	struct stat		st;
	register char	*str;

	if (!(str = ft_strjoin(array->pguitar.comp.dirr, d_name)))
		ft_42sh_exit(E_MEM_CODE_42SH);
	lstat(str, &st);
	free(str);
	str = NULL;	
	if (S_ISLNK(st.st_mode))
		//ft_42sh_add_symb(d_name, "@", array);
		ft_42sh_auto_add_list(array, d_name, '@');
	else if (S_ISDIR(st.st_mode))
		//ft_42sh_add_symb(d_name, "/", array);
		ft_42sh_auto_add_list(array, d_name, '/');
	else if (st.st_mode & S_IXUSR || st.st_mode & S_IXGRP || st.st_mode & S_IXOTH)
		//ft_42sh_add_symb(d_name, "*", array);
		ft_42sh_auto_add_list(array, d_name, '*');
	else
		ft_42sh_auto_add_list(array, d_name, 0);
	return (1);
}

size_t	ft_42sh_search_cmp(register t_main_42sh *array, char *d_name)//ищем совпадение файла в директории с array->pguitar.comp.str
{
	if (ft_strncmp(d_name, array->pguitar.comp.str, array->pguitar.comp.count_str) == 0)
		ft_42sh_type_file(d_name, array);
	else	
		return (0);
	return (1);
}


void	ft_42sh_add_files(t_main_42sh *array)//читаем содержимое директории, заполняем листы, создаем массив
{
	DIR				*dir;
	struct dirent	*entry;

	if (!(dir = opendir(array->pguitar.comp.dirr)))
		return ;
	while ((entry = readdir(dir)) != NULL)
	{
		if ((entry->d_name[0] == '.' && entry->d_name[1] == '\0') || 
			(entry->d_name[0] == '.' && entry->d_name[1] == '.' && entry->d_name[2] == '\0'))
			;
		else if (*(array->pguitar.comp.str) != '\0')
			array->pguitar.comp.sum += ft_42sh_search_cmp(array, entry->d_name);
		else if ((entry->d_name[0] != '.'))
			array->pguitar.comp.sum += ft_42sh_type_file(entry->d_name, array);
	}
	closedir(dir);
	ft_42sh_auto_create_array(array);
}

size_t	ft_42sh_is_dir(char *dir)// проверяем существование директорий
{
	struct stat	st;
	
	lstat(dir, &st);
	if (S_ISDIR(st.st_mode))
		return (1);
	else
		return (0);
}

void	ft_42sh_fill_dir(register t_main_42sh *array, char *tmp, size_t i)// заполняем dirr
{
	register size_t	j;
	register size_t	k;
	register char	*str;

	j = -1;
	k = 0;
	str = array->pguitar.comp.str;
	if (!(tmp = (char *)malloc(sizeof(char) * (array->pguitar.comp.count_dirr + i + 1))))
		ft_42sh_exit(E_MEM_CODE_42SH);
	while (++j < array->pguitar.comp.count_dirr)
		*(tmp + j) = *(array->pguitar.comp.dirr + j);
	while (k++ < i)
	{
		*(tmp + j) = *str;
		str++;
		j++;
	}
	*(tmp + j) = '/';
	if (ft_42sh_is_dir(tmp))
	{
		free(array->pguitar.comp.dirr);
		array->pguitar.comp.dirr = tmp;
		array->pguitar.comp.str += i + 1;
		array->pguitar.comp.count_str -= i + 1;
		array->pguitar.comp.count_dirr += i + 1;
	}
	else
	{
		free(array->pguitar.comp.dirr);
		array->pguitar.comp.dirr = NULL; //???
	}
}

void	ft_42sh_check_dir(register t_main_42sh *array)// проверяем существование директорий
{
	register size_t	i;
	register char	*tmp;
	register char	*str;

	i = 0;
	tmp = NULL;
	str = array->pguitar.comp.str;
	while (*str)
	{
		if (ft_42sh_is_dir(array->pguitar.comp.dirr))
		{
			while (*str != '\0' && *str != '/' && ++i)
				str++;
			if (*str == '/')
			{
				ft_42sh_fill_dir(array , tmp, i);
				str++;
				i = 0;
			}
		}
	}
}

void	ft_42sh_dir(register t_main_42sh *array)// ищем в строке директорию и символы после директории
{
	if (array->pguitar.comp.count_str == 0 || (*(array->pguitar.comp.str) != '/'))
	{
		if (!(array->pguitar.comp.dirr = (char *)malloc(sizeof(char) * 2)))
			ft_42sh_exit(E_MEM_CODE_42SH);
		*(array->pguitar.comp.dirr) = '.';
		*(array->pguitar.comp.dirr + 1) = '/';
		array->pguitar.comp.count_dirr = 2;
		if (*(array->pguitar.comp.str) == '.' && *(array->pguitar.comp.str + 1) == '/')
		{
			array->pguitar.comp.str = array->pguitar.comp.str + 2;
			array->pguitar.comp.del_dirr += 2;
			array->pguitar.comp.count_str = array->pguitar.comp.count_str - 2;
		}
	}
	else
	{
		if (!(array->pguitar.comp.dirr = (char *)malloc(sizeof(char) * 1)))
			ft_42sh_exit(E_MEM_CODE_42SH);
		*(array->pguitar.comp.dirr) = '/';
		array->pguitar.comp.count_dirr = 1;
		array->pguitar.comp.str++;
		array->pguitar.comp.del_dirr--;//???
		array->pguitar.comp.count_str--;
	}
	*(array->pguitar.comp.dirr + array->pguitar.comp.count_dirr) = '\0';
	ft_42sh_check_dir(array);
	ft_42sh_add_files(array);
}

size_t	ft_42sh_auto_diff_quote(register char *cur, register t_main_42sh *array)
{
	register char	*tmp_cur;

	tmp_cur = cur;
	while (*tmp_cur)
	{
		if (array->pguitar.comp.type_q == 0 && *tmp_cur == '\'')
			array->pguitar.comp.type_q = 1;
		else if (array->pguitar.comp.type_q == 0 && *tmp_cur == '\"')
			array->pguitar.comp.type_q = 2;
		else if (array->pguitar.comp.type_q != 1 && *tmp_cur == '\'')
			return (0);
		else if (array->pguitar.comp.type_q != 2 && *tmp_cur == '\"')
			return (0);
		tmp_cur++;
	}
	return (1);
}

size_t	ft_42sh_pars_quote(register char *cur, register char *tmp_cur, 
		register t_main_42sh *array, register size_t i)
{
	register size_t	j;
	register char	*str;

	j = 0;
	array->pguitar.comp.del_str = i;
	if (!(ft_42sh_auto_diff_quote(cur, array)))
		return (0);
	if (!(str = (char *)malloc(sizeof(char) * i)))
		ft_42sh_exit(E_MEM_CODE_42SH);
	tmp_cur = cur;
	array->pguitar.comp.cur = tmp_cur;
	while (j < i && *(tmp_cur + j) != '\0')
	{
		while (*(tmp_cur + j) != '\0' && (*(tmp_cur + j) == '\'' || *(tmp_cur + j) == '\"') && i--)
			tmp_cur++;
		*(str + j) = *(tmp_cur + j);
		j++;
	}
	array->pguitar.comp.count_str = i;
	array->pguitar.comp.del_dirr = i; //если 0, то вывести все в текущей директории, если не ноль, то вывести в какой-либо директории или дополнить директорию/файл
	array->pguitar.comp.str = str;
	return (1);
}

size_t	ft_42sh_pars_str(register char *cur, register char *tmp_cur,
		register char *b, register t_main_42sh *array)
{
	register size_t i;

	i = 0;
	while (cur != b)
	{
		if ((*(cur) == ' ' && *(cur - 1) != '\\') || (*(cur) == '\0' && *(cur - 1) == ' '))
			break ;
		else
			cur--;
		i++;
	}
	if ((*cur) == ' ' && i > 1 && i--)
		cur++;
	while (*tmp_cur != '\0')
	{
		if ((*(cur) == ' ' && *(cur - 1) != '\\') || (*(tmp_cur) == '\\' &&
			(*(tmp_cur + 1) != ' ' && *(tmp_cur + 1) != '\0')))
			break ;
		else
			tmp_cur++;
		i++;
	}
	if (!ft_42sh_pars_quote(cur, cur, array, i))
		return (0);
	return (1);
}
void	ft_42sh_init_struct(register t_main_42sh *array,
		register t_in_42sh *list)//инициализируем структуру
{
	register size_t	i;
	register char	*b;
	register char	*cur;
	register char	*tmp_cur;

	i = 0;
	b = list->lp_b;
	*(list->lp_b + list->count) = '\0';
	cur = list->lp_current;
	array->pguitar.comp.cur = cur;
	array->pguitar.comp.sum = 0;
	array->pguitar.comp.type_q = 0;
	array->pguitar.comp.del_str = 0;
	array->pguitar.comp.del_dirr = 0;
	tmp_cur = cur;
	if (!ft_42sh_pars_str(cur, tmp_cur, b, array))
		return ;
	if (array->lp_auto->all_cmd.first == 0)
		ft_42sh_dir(array);
	if ((*(array->pguitar.comp.str) == '\0' || array->pguitar.comp.sum != 1) && array->pguitar.comp.sum != 0)
		ft_42sh_auto_cmd(array, list, 0, 0);
	else /*if (array->pguitar.comp.sum != 0)*/
		ft_42sh_auto_cmd(array, list, (void*)(array->pguitar.comp.cur + array->pguitar.comp.count_dirr), array->pguitar.comp.count_str);
}

void	ft_42sh_auto_comp(register t_main_42sh *array,
		register t_in_42sh *list) //??? добавить auto к именам функций
{
	if (list->lp_b == list->lp_current) //??? изменить
		return ;
	ft_42sh_init_struct(array, list);
}
