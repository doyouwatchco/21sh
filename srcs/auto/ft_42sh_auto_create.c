/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_auto_create.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static t_all_cmd_42sh		*fn_list_create(register t_main_42sh *array,
register char *key, register char add_litter)
{
	register size_t				key_count;
	register size_t				key_litter;
	register t_all_cmd_42sh		*out;
	register char				lit;

	key_count = 0;
	key_litter = 0;
	while ((lit = key[key_count++]) != 0)
	{
		key_litter++;
		if ((lit & 0x80) != 0)
		{
			while (((lit = key[key_count]) & 0x80) != 0 && (lit & 0x40) == 0)//Учитываем utf-8 что бы знать на сколько байт сместить в право
				key_count++;
		}
	}
	if ((out = malloc(sizeof(t_all_cmd_42sh) + key_count)) == 0)//Ключ храним в самом конце структуры - что бы лишний раз не выделять память
		ft_42sh_exit(E_MEM_CODE_42SH);
	out->std.key_count = key_count - 1;
	out->std.lp_key = out->key;
	out->key_litter = key_litter;
	out->add_litter = add_litter;
	key_litter += add_litter != 0 ? 1 : 0;
	array->lp_auto->max_litter = key_litter > array->lp_auto->max_litter ? key_litter : array->lp_auto->max_litter;
	ft_memcpy(out->key, key, key_count);
	return (out);
}

void					*ft_42sh_auto_add_list(register t_main_42sh *array,
register char *lp_key, char add_litter)
{
	register t_past_sort_42sh	*root;
	register size_t				tempos;
	register t_all_cmd_42sh		*list;

	root = &array->lp_auto->all_cmd;
	list = fn_list_create(array, lp_key, add_litter);//Создадим лист
	if (root->first == 0)//Если не записано то вставим лист без поиска
	{
		list->std.next = 0;
		list->std.prev = 0;
		root->first = list;
		root->center = list;
		root->last = list;
		tempos = 1;
	}
	else
		tempos = ft_42sh_list_sort_paste(root, &list->std, free);//Добавим лист методом ставки в алфавитном порядке
	if (tempos == 0)
		return (0);
	array->lp_auto->count_all++;
	return (list);
}

static void					fn_while_dir(register t_main_42sh *array,
register char **spl)
{
	register char			*tmp;
	register size_t			tempos;
	register DIR			*dirp;
	register struct dirent	*dp;
	struct stat				st;

	while ((tmp = spl++[0]) != 0)
	{
		if ((dirp = opendir(tmp)) != 0)
		{
			chdir(tmp);//Для удобства сделаем текущий каталог другим - чтобы каждый раз пути чрез маллок не склеивать
			while ((dp = readdir(dirp)) != 0)
			{
				if (lstat((tmp = dp->d_name), &st) == 0)
				{
					tempos = st.st_mode;//По правильному надо бы учитывать вазможность ссылак и уже по исходному файлу определять исполняемый он или нет
					if ((tempos & S_IFMT) == S_IFREG && ((tempos & S_IXUSR) |
					(tempos & S_IXGRP) | (tempos & S_IXOTH) != 0))
						ft_42sh_auto_add_list(array, tmp, 0);//добавим в массив который состоит из анси символов листы - для ускарения
				}
			}
			closedir(dirp);
		}
	}
}

void						ft_42sh_auto_create_array(register t_main_42sh *array)
{
	register t_all_cmd_42sh		*list;
	register size_t				tempos;
	register t_all_cmd_42sh		**spl;

	list = array->lp_auto->all_cmd.first;
	tempos = array->lp_auto->count_all;
	if ((spl = malloc(sizeof(t_all_cmd_42sh *) * (tempos + 1))) == 0)
		ft_42sh_exit(E_MEM_CODE_42SH);
	spl[tempos] = 0;
	array->lp_auto->spl_all_cmd = spl;
	while (list != 0)
	{
		spl++[0] = list;
		list = (void *)list->std.next;
	}
}

void						ft_42sh_auto_create(register t_main_42sh *array)
{
	register char		**spl;

	array->lp_auto->count_all = 0;
	array->lp_auto->max_litter = 0;
	if ((spl = array->lp_spl_path) != 0)//Предыдущий массив с путями к исполняемым файлам освобождаем
		ft_strsplit_free(spl);
	if ((spl = (char **)array->env.lp_env_path) != 0)//Если переменная среды PATH есть то создадим списак с всеми исполняемыми файлами
	{
		if ((spl = ft_strsplit((((t_env_42sh *)spl)->lp_value), ':')) == 0)
			ft_42sh_exit(E_MEM_CODE_42SH);
		fn_while_dir(array, spl);//Переберем все дериктории
		chdir(array->pwd.path.buff);//Востановим текущий рабочий каталог
	}
	array->lp_spl_path = spl;
	ft_42sh_auto_add_list(array,"setenv", 0);
	ft_42sh_auto_add_list(array, "unsetenv", 0);
	ft_42sh_auto_add_list(array, "echo", 0);
	ft_42sh_auto_add_list(array, "env", 0);
	ft_42sh_auto_add_list(array, "exit", 0);
	ft_42sh_auto_add_list(array, "cd", 0);
	ft_42sh_auto_add_list(array, "clear", 0);
	ft_42sh_auto_add_list(array, "21sh", 0);
	ft_42sh_auto_add_list(array, "21shdSYM", 0);
	ft_42sh_auto_create_array(array);
}
