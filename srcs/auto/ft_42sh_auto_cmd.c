/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_auto_cmd.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/21 21:58:20 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:21:12 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_42sh.h"

static void					fn_add_str(register t_main_42sh *array,
register t_in_42sh *list, register t_all_cmd_42sh *cmd, register size_t n)
{
	t_add_litter_42sh			in;

	in.count = cmd->std.key_count - n;
	in.count_litter = cmd->key_litter - array->lp_auto->auto_litter_len;
	ft_42sh_str(array, list, (void *)(cmd->std.lp_key + n), &in);
	in.count = 1;
	in.count_litter = 1;
	ft_42sh_str(array, list, (void *)" ", &in);
}

static t_all_cmd_42sh		**fn_test(register t_main_42sh *array,
register t_in_42sh *list, register t_all_cmd_42sh **spl,
register t_all_cmd_42sh **spl_end)
{
	t_add_litter_42sh				in;
	register t_all_cmd_42sh			*cmd;
	register size_t					n;

	spl_end--;
	cmd = spl[0];
	n = array->lp_auto->auto_len;
	if (cmd->std.key_count == n && spl != spl_end)
		cmd = spl[1];
	if (cmd == spl_end[0])
	{
		fn_add_str(array, list, cmd, n);
		return (0);
	}
	if (spl_end[0]->std.key_count >= cmd->std.key_count &&
	ft_strncmp(spl_end[0]->key, cmd->key, cmd->std.key_count) == 0)
	{
		in.count = cmd->std.key_count - n;
		in.count_litter = cmd->key_litter - array->lp_auto->auto_litter_len;
		ft_42sh_str(array, list, (void *)(cmd->std.lp_key + n), &in);
		return (0);
	}
	spl_end += spl_end[1] != 0 ? 1 : 0;
	return (spl_end);
}

static t_all_cmd_42sh		**fn_start(register t_main_42sh *array,
register t_in_42sh *list, register unsigned char *str, register size_t n)
{
	register t_all_cmd_42sh			**spl;
	register size_t					count;

	array->lp_auto->auto_len = n;
	if (str == 0)
	{
		array->lp_auto->auto_litter_len = 0;
		ft_42sh_auto_limit(array, array->lp_auto->max_litter,
		array->lp_auto->spl_all_cmd, array->lp_auto->count_all);
		return (0);
	}
	if ((spl = (t_all_cmd_42sh **)
	ft_42sh_spl_find((void **)array->lp_auto->spl_all_cmd,
	array->lp_auto->count_all, (char *)str, n)) == 0)
	{
		ft_write_buffer_str_zero(&array->out, "\x7");//Звук когда такого корня ключа не имеем
		return (0);
	}
	array->lp_auto->auto_litter_len = ft_strlen_utf8_n((char *)str,
	(char *)(str + n));
	count = list->lp_current - (char *)str;
	if ((n -= count) != 0)
	{
		str += count;
		ft_42sh_dsp_caret_right(array, list, ft_strlen_utf8_n((char *)str, (char *)(str + n)), n);
	}
	return (spl);
}

void						ft_42sh_auto_cmd(register t_main_42sh *array,
register t_in_42sh *list, register unsigned char *str, register size_t n)//выведет все команды возможные
{
	register t_all_cmd_42sh			**spl;
	register t_all_cmd_42sh			**spl_end;
	register t_all_cmd_42sh			*cmd;
	register size_t					tempos;

	if ((spl = fn_start(array, list, str, n)) == 0)
		return ;
	spl_end = spl + 1;
	tempos = spl[0]->key_litter;
	while ((cmd = spl_end[0]) != 0 &&
	ft_strncmp((char *)str, cmd->key, n) == 0)
	{
		if (cmd->key_litter > tempos)
			tempos = cmd->key_litter;
		spl_end++;
	}
	if ((spl_end = fn_test(array, list, spl, spl_end)) == 0)
		return ;
	ft_42sh_auto_limit(array, tempos, spl, spl_end - spl);
}
