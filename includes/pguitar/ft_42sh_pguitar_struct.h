/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_42sh_pguitar_struct.h                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: amatilda <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/17 12:06:23 by amatilda          #+#    #+#             */
/*   Updated: 2019/06/25 15:24:38 by amatilda         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_42SH_PGUITAR_STRUCT_H
# define FT_42SH_PGUITAR_STRUCT_H

typedef struct				s_pguitar_comp_42sh
{
	char					*b;
	char					*cur;
	char					*str;
	char					*dirr;
	size_t					sum;
	size_t					type_q;
	size_t					del_str;
	size_t					del_dirr;
	size_t					count_str;
	size_t					count_dirr;
}							t_pguitar_comp_42sh;

typedef struct				s_pguitar_42sh//Все структуры - pguitar
{
	t_pguitar_comp_42sh		comp;
}							t_pguitar_42sh;

#endif
